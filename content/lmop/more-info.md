---
title: "More Info"
draft: false
logo: https://wildwoodhealth.com/wp-content/uploads/2019/06/Small-Logo-e1561060480358.png
greeting: "Hi,"
---

We are exicted you have taken the first step in joining the Wildwood Lifestyle Medicine Observer Program!

To get you started we have provided the course curriculum & handbook which contain the specific details of your 4-week training program.

Ready to apply? Click here: [observer.wildwoodhealth.com/apply](https://observer.wildwoodhealth.com/apply)

If you have any more questions about the program, just reply to this email or give us a call at [(706) 820-7457](tel:17068207457).

**Curriculum:**  
[observer.wildwoodhealth.com/curriculum/](https://observer.wildwoodhealth.com/curriculum/)

**Handbook:**  
[observer.wildwoodhealth.com/handbook/](https://observer.wildwoodhealth.com/handbook/)
