---
title: "Health Evangelism Online"
draft: false
logo: https://healthevangelism.com/wp-content/uploads/2019/06/Education-Logo-e1561059765249.png
greeting: Hello First,
---

Thank you for enrolling in our online Health Evangelism course. We are processing your information and setting up your online school account. Your username and password will be send to you soon. Please check your junk/spam mail in case this email accidentally ends up there.

We pray for God’s blessings as you take this course. Feel free to contact us at [onlineschool@healthevangelism.com](mailto:onlineschool@healthevangelism.com) if you have any questions.

God bless,  
Wildwood Center for Health Evangelism
