---
title: "Health Evangelism Advanced"
draft: false
logo: https://healthevangelism.com/wp-content/uploads/2019/06/Education-Logo-e1561059765249.png
greeting: Hello First,
---
We are excited to hear that you want to apply for the Lifestyle Coaching course!

In order to complete the application process, please submit your medical form by scanning and emailing the completed form to [registrar@healthevangelism.com](mailto:registrar@healthevangelism.com). We will be able to review your application only when we have received this document. You can expect a response to your application the beginning of October, if you have applied for the spring session, or the beginning of April if you have applied for the fall session.

We look forward to possibly have you come study at Wildwood Center for Health Evangelism!

God bless,  
Wildwood Center for Health Evangelism
