---
title: "Intensive Week"
draft: false
logo: https://healthevangelism.com/wp-content/uploads/2019/06/Education-Logo-e1561059765249.png
greeting: Hello First,
---

We would like to let you know that we have received your payment for the week-long intensive hydrotherapy and massage training. We look forward to seeing you during this upcoming session.

Please contact us at [onlineschool@healthevangelism.com](mailto:onlineschool@healthevangelism.com) or at [706-419-0030](tel:+17064190030,1245) ext. 1245 regarding your travel plans, so that we can arrange a pick-up.

God bless,  
Wildwood Center for Health Evangelism
