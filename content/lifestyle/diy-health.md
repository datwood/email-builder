---
title: "DIY Health"
draft: false
logo: https://wildwoodhealth.com/wp-content/uploads/2019/06/Small-Logo-e1561060480358.png
greeting: Hey there! Here's your 10% discount at Wildwood Lifestyle Center.
---

Use this coupon when you sign up for one of our 11-day residential programs ([Disease Reversal](https://wildwoodhealth.com/programs/life-alignment/), [Depression Recovery](https://wildwoodhealth.com/programs/depression-recovery/), or [Life Alignment](https://wildwoodhealth.com/programs/life-alignment/)) and let us help you boost your immune system to fight off the coronavirus.

Call now at [800-634-9355](tel:800-634-9355) and reserve your spot!

{{< img alt="Coupon" width="300px" src="https://wildwoodhealth.com/wp-content/uploads/2020/04/april-coupon.png" >}}

{{< small >}}
*This coupon is only valid for our residential programs in April 2020.
{{< /small >}}
