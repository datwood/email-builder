var gulp = require('gulp'),
    exec = require('child_process').exec,
    inlineCss = require('gulp-inline-css'),
    prettyHtml = require('gulp-pretty-html');

gulp.task('hugo', function () {
    exec('rm -rf public')
    return exec('hugo')
})

gulp.task('inlineCss', function () {
    return gulp.src('public/**/*.html')
        .pipe(inlineCss({
            applyStyleTags: true,
            applyLinkTags: false,
            removeStyleTags: true,
            removeLinkTags: false,
            preserveMediaQueries: false,
            applyWidthAttributes: true,
            applyTableAttributes: true,
            removeHtmlSelectors: true
        }))
        .pipe(gulp.dest('public/'))
})

gulp.task('pretty', function () {
    return gulp.src('public/**/*.html')
        .pipe(prettyHtml({
            preserve_newlines: false
        }))
        .pipe(gulp.dest('public'))
})

gulp.task('default', gulp.series('hugo', 'inlineCss', 'pretty'))